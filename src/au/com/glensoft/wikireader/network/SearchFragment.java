package au.com.glensoft.wikireader.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

/**
 * This Fragment manages a single background task and retains 
 * itself across configuration changes.
 */
public class SearchFragment extends Fragment {

  /**
   * Callback interface through which the fragment will report the
   * task's progress and results back to the Activity.
   */
  public static interface TaskCallbacks {
    void onPostExecute(List<List<String>> feed);
  }

  public String searchTerms;
  private TaskCallbacks mCallbacks;
  private DummyTask mTask;

  /**
   * Hold a reference to the parent Activity so we can report the
   * task's current progress and results. The Android framework 
   * will pass us a reference to the newly created Activity after 
   * each configuration change.
   */
  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    mCallbacks = (TaskCallbacks) activity;
  }

  /**
   * This method will only be called once when the retained
   * Fragment is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Retain this fragment across configuration changes.
    setRetainInstance(true);

    // Create and execute the background task.
    mTask = new DummyTask();
    String searchTerm = "";
    String[] array = searchTerms.split(" ");
    for (int i = 0;i < array.length; i++) {
    	if (i == 0) {
    		searchTerm = array[i];
    	} else {
    		searchTerm += "+" + array[i];
    	}
    }
    mTask.execute(searchTerm);
  }

  /**
   * Set the callback to null so we don't accidentally leak the 
   * Activity instance.
   */
  @Override
  public void onDetach() {
    super.onDetach();
    mCallbacks = null;
  }

  /**
   * A dummy task that performs some (dumb) background work and
   * proxies progress updates and results back to the Activity.
   *
   * Note that we need to check if the callbacks are null in each
   * method in case they are invoked after the Activity's and
   * Fragment's onDestroy() method have been called.
   */
  private class DummyTask extends AsyncTask<String, Void, List<List<String>>> {

    @Override
    protected void onPreExecute() {
    }

    /**
     * Note that we do NOT call the callback object's methods
     * directly from the background thread, as this could result 
     * in a race condition.
     */
    @Override
    protected List<List<String>> doInBackground(String... searchTerm) {
    	List<String> urls = new ArrayList<String>();
    	List<String> titles = new ArrayList<String>();
    	try {
    		Document doc = Jsoup.connect("http://en.wikipedia.org/w/index.php?search=" + searchTerm[0]).timeout(10*1000).get();
    		
    		if (doc.location().contains("search=")) {
    		
    		Elements links = doc.getElementsByClass("mw-search-result-heading");
    		//Elements html = Jsoup.connect("http://www.reddit.com/domain/en.wikipedia.org/").get().getElementsByClass("title ");
    		for (Element element : links) {
    			String link = "http://en.wikipedia.org" + element.getElementsByTag("a").attr("href");
    			String title = element.getElementsByTag("a").text();
    			urls.add(link);
    			titles.add(title);
    		}
    		
        	} else {
    			String link = doc.location();
    			String title = doc.getElementsByTag("h1").first().text();
    			urls.add(link);
    			titles.add(title);
        	}
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}

    	List<List<String>> returnValue = new ArrayList<List<String>>();
    	returnValue.add(urls);
    	returnValue.add(titles);
    	return returnValue;
    }

    @Override
    protected void onCancelled() {
    }

    @Override
    protected void onPostExecute(List<List<String>> feed) {
      if (mCallbacks != null) {
        mCallbacks.onPostExecute(feed);
      }
    }
  }
}