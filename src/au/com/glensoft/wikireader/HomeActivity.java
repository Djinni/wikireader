package au.com.glensoft.wikireader;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import au.com.glensoft.wikireader.network.TaskFragment;

public class HomeActivity extends Activity {
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        
        Button articleButton = (Button) findViewById(R.id.articleButton);
        articleButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
        	
        });
        
        Button redditButton = (Button) findViewById(R.id.redditButton);
        redditButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent myIntent = new Intent(HomeActivity.this, ArticleChoiceActivity.class);
				HomeActivity.this.startActivity(myIntent);
			}
        });
        
        final EditText searchText = (EditText) findViewById(R.id.searchText);
        Button searchButton = (Button) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String searchString = searchText.getText().toString();
				Intent myIntent = new Intent(HomeActivity.this, SearchResultsActivity.class);
				myIntent.putExtra("searchterm", searchString);
				HomeActivity.this.startActivity(myIntent);
			}
        	
        });
    }

}
