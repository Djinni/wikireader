package au.com.glensoft.wikireader;

import java.util.HashMap;
import java.util.List;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import au.com.glensoft.wikireader.network.ReadFragment;

public class ReadAllActivity extends FragmentActivity implements ReadFragment.TaskCallbacks {

	private int MY_DATA_CHECK_CODE = 0;
	
	private int speechIndex = 3;
	
	List<String> text;
    
    private ReadFragment mTaskFragment;
    
   

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    	Intent checkTTSIntent = new Intent();
    	checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
    	startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
        
        setContentView(R.layout.read_all_article);
        
        Button skipButton = (Button) this.findViewById(R.id.button1);
        skipButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((App)ReadAllActivity.this.getApplication()).myTTS.stop();	
			}
        });
        
        Button stopButton = (Button) this.findViewById(R.id.button2);
        stopButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((App)ReadAllActivity.this.getApplication()).speeches.clear();	
				((App)ReadAllActivity.this.getApplication()).myTTS.stop();	
			}
        });
        
        startTask();
        
    }
    
    private void startTask() {
    	if (this.getIntent().getStringArrayExtra("urls").length > speechIndex) {
	        android.app.FragmentManager fm = getFragmentManager();
	        mTaskFragment = (ReadFragment) fm.findFragmentByTag("task");
	
	        if (mTaskFragment == null) {
	          mTaskFragment = new ReadFragment();
	          Bundle bundle = new Bundle();
	          bundle.putString("url", this.getIntent().getStringArrayExtra("urls")[speechIndex]);
	          speechIndex++;
	          mTaskFragment.setArguments(bundle);
	          fm.beginTransaction().add(mTaskFragment, "task" + speechIndex).commit();
	        }
    	}
    }
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
      super.onSaveInstanceState(savedInstanceState);
      savedInstanceState.putInt("speechIndex", speechIndex);
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
      super.onRestoreInstanceState(savedInstanceState);
      speechIndex = savedInstanceState.getInt("speechIndex");
    }

    @Override
    public void onPostExecute(List<String> text) { 
    	
    	this.text = text;
    	for (String speech : text) {
    		((App)this.getApplication()).speeches.add(speech);//.myTTS.speak(speech, TextToSpeech.QUEUE_ADD, null);
    		
    	}
    	if (speechIndex == 4) {
    		HashMap<String, String> params = new HashMap<String, String>();

    		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,"stringId");
    		((App)this.getApplication()).myTTS.stop();
    		((App)this.getApplication()).myTTS.speak("a", TextToSpeech.QUEUE_FLUSH, params);
    	}
    	android.app.FragmentManager fm = getFragmentManager();
        mTaskFragment = (ReadFragment) fm.findFragmentByTag("task" + speechIndex);
        fm.beginTransaction().remove(mTaskFragment).commit();
        startTask();
    	
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode != TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.article_choice, menu);
        return true;
    }
}
