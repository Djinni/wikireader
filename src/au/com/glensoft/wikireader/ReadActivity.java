package au.com.glensoft.wikireader;

import java.util.List;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import au.com.glensoft.wikireader.network.ReadFragment;

public class ReadActivity extends FragmentActivity implements ReadFragment.TaskCallbacks {

	private int MY_DATA_CHECK_CODE = 0;
	
	List<String> text;
    
    private ReadFragment mTaskFragment;
    
   

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    	Intent checkTTSIntent = new Intent();
    	checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
    	startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
        
        setContentView(R.layout.read_article);
        
        Button stopButton = (Button) this.findViewById(R.id.button1);
        stopButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((App)ReadActivity.this.getApplication()).myTTS.stop();	
			}
        });
        
        android.app.FragmentManager fm = getFragmentManager();
        mTaskFragment = (ReadFragment) fm.findFragmentByTag("task");

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (mTaskFragment == null) {
          mTaskFragment = new ReadFragment();
          Bundle bundle = new Bundle();
          bundle.putString("url", this.getIntent().getStringExtra("url"));
          mTaskFragment.setArguments(bundle);
          fm.beginTransaction().add(mTaskFragment, "task").commit();
        }
        
    }

    @Override
    public void onPostExecute(List<String> text) { 
    	
    	System.out.println(text);
    	this.text = text;
    	for (String speech : text) {
    		((App)this.getApplication()).myTTS.speak(speech, TextToSpeech.QUEUE_ADD, null);
    	}
    	
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode != TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }
    


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.article_choice, menu);
        return true;
    }
}
