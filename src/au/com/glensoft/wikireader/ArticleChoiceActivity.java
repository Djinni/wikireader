package au.com.glensoft.wikireader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import au.com.glensoft.wikireader.network.TaskFragment;

public class ArticleChoiceActivity extends FragmentActivity implements TaskFragment.TaskCallbacks {

    
    private TaskFragment mTaskFragment;
    
    public List<String> feeds;
    public List<String> titles;
    
    public ListView listView;
    
    public boolean loading = true;
    
    private ProgressDialog progress;
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	if (progress != null && progress.isShowing()) {
    		progress.dismiss();
    	}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        

        
    	feeds = new ArrayList<String>();
    	titles = new ArrayList<String>();
    	if (savedInstanceState != null) {
	        feeds = savedInstanceState.getStringArrayList("feeds");
	        titles = savedInstanceState.getStringArrayList("titles");
	        loading = savedInstanceState.getBoolean("loading");
    	}
    	
    	if (loading) {
    		progress = new ProgressDialog(this);
    		progress.setTitle("Loading");
    		progress.setMessage("Wait while loading...");
    		progress.show();
    	}
        
        setContentView(R.layout.activity_article_choice);
        
        //setup list view
        listView = (ListView) this.findViewById(R.id.listView1);
        if (feeds != null) {
            ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
            		android.R.id.text1, titles);
            listView.setAdapter(modeAdapter);
            
        }
        listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Intent myIntent = new Intent(ArticleChoiceActivity.this, ReadActivity.class);
				myIntent.putExtra("url", feeds.get(position));
				ArticleChoiceActivity.this.startActivity(myIntent);
			}
        	
        });
        
        //download data if needed.
        if (loading) {
	        android.app.FragmentManager fm = getFragmentManager();
	        mTaskFragment = (TaskFragment) fm.findFragmentByTag("task");
	
	        // If the Fragment is non-null, then it is currently being
	        // retained across a configuration change.

	        if (mTaskFragment == null) {
	          mTaskFragment = new TaskFragment();
	          fm.beginTransaction().add(mTaskFragment, "task").commit();
	        }
        }
        
    }
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
      super.onSaveInstanceState(savedInstanceState);
      savedInstanceState.putStringArrayList("feeds", (ArrayList<String>) feeds);
      savedInstanceState.putStringArrayList("titles", (ArrayList<String>) titles);
      savedInstanceState.putBoolean("loading", loading);
      
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
      super.onRestoreInstanceState(savedInstanceState);

    }

    @Override
    public void onPostExecute(List<List<String>> feed) { 
    	loading = false;
    	feeds = feed.get(0);
    	titles = feed.get(1);
    	ArrayAdapter<String> adapter = (ArrayAdapter<String>) listView.getAdapter();
    	adapter.clear();
    	adapter.addAll(titles);
    	adapter.notifyDataSetChanged();
    	
    	if (progress != null && progress.isShowing()) {
    		progress.dismiss();
    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.article_choice, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
	    switch (item.getItemId()){
	          case R.id.action_settings:
		          //startActivity (new Intent (this, PrefsActivity.class));
		          break;
	          case R.id.action_read_all:
					Intent myIntent = new Intent(ArticleChoiceActivity.this, ReadAllActivity.class);
					myIntent.putExtra("urls", feeds.toArray(new String[feeds.size()]));
					ArticleChoiceActivity.this.startActivity(myIntent);
	        	  break;
	    }
	    return true;
    }

    public class feedAdapter extends ArrayAdapter<String> {
    	
    	// declaring our ArrayList of items
    	public ArrayList<String> objects;

    	public feedAdapter(Context context, int textViewResourceId, ArrayList<String> objects) {
    		super(context, textViewResourceId, objects);
    		this.objects = objects;
    	}
    	
    	/*
    	 * we are overriding the getView method here - this is what defines how each
    	 * list item will look.
    	 */
    	public View getView(int position, View convertView, ViewGroup parent){
    		View v = convertView;

    		// first check to see if the view is null. if so, we have to inflate it.
    		// to inflate it basically means to render, or show, the view.
    		if (v == null) {
    			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    			v = inflater.inflate(android.R.layout.simple_list_item_1, null);
    			
    			TextView text = (TextView) v.findViewById(android.R.id.text1);
    			text.setText(objects.get(position));
    		}

    		// the view must be returned to our activity
    		return v;

    	}
    	
    }


}
