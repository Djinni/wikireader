package au.com.glensoft.wikireader;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

import android.app.Application;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.widget.Toast;

public class App extends Application implements OnInitListener, TextToSpeech.OnUtteranceCompletedListener {
	
	 public TextToSpeech myTTS;
	 
	 public boolean speaking = false;
	 
	 public LinkedList<String> speeches = null;
	
	@Override
	public void onCreate() {
		super.onCreate();
		speeches = new LinkedList<String>();
		myTTS = new TextToSpeech(this, this);
		
		//start download
	}
	
    public void onInit(int initStatus) {
        if (initStatus == TextToSpeech.SUCCESS) {
            myTTS.setLanguage(Locale.US);
        } else if (initStatus == TextToSpeech.ERROR) {
            Toast.makeText(this, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
        }
        myTTS.setOnUtteranceCompletedListener(this);
    }
    
    public void onUtteranceCompleted(String utteranceId) {
    	Log.i("WIKIREAD", utteranceId);
    	if (speeches != null && speeches.size() > 0) {
    		HashMap<String, String> params = new HashMap<String, String>();

    		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,"stringId");
    		
    		 String speech = speeches.removeFirst();
    		 myTTS.speak(speech, TextToSpeech.QUEUE_ADD, params);
    	}
    	
    }

}
